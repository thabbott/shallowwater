#!/bin/bash

# Set search path before starting notebook
export PYTHONPATH="/data/thabbott/dev/grids:/data/thabbott/dev/shallowwater:$PYTHONPATH"
jupyter notebook
