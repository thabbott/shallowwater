"""
Shallow water solvers in various geometries
"""
import arakawa
import numpy as np
import matplotlib.pyplot as plt

class R1(object):
    """
    Shallow water solver for a 1D radially symmetric system. See the
    documentation for details about the equation system and discretization
    strategy.
    """

    def __init__(self, rmax, N, f, H0, grav = 9.81):
        """
        Parameters
        ----------
        rmax: float
            Maximum distance from the origin represented on the grid
        N: float
            Number of points in the grid
        f: float
            Coriolis parameter (units 1/s)
        H0: float
            Fluid depth outside the model domain (units m)
        grav: float, optional
            Gravitational acceleration (units m/s^2), default 9.81
        """
        self.g = arakawa.C1(np.linspace(0, rmax, N), tracer = True)
        """ Model grid """
        self.hvrt = np.tile(self.g.u(), [3, 1])
        """ Radial transport tendency (m*m/s/s) """
        self.hvtt = np.tile(self.g.u(), [3, 1])
        """ Azimuthal transport tendency (m*m/s/s) """
        self.ht = np.tile(self.g.q(), [3, 1])
        """ Depth tendency (m/s) """
        self.f = f
        """ Coriolis parameters (1/s) """
        self.H0 = H0
        """ Fluid depth outside of the model domain (m) """
        self.grav = grav
        """ Gravitational acceleration (m/s^2) """
        self.n = 0
        """ Model time step (nondim) """
        self.t = 0
        """ Model time (s) """

        # Append ghost cells and create field arrays
        self.g.xu = np.insert(self.g.xu, 0, -self.g.xu[0])
        self.g.xu = np.append(self.g.xu, 2*self.g.xu[-1] - self.g.xu[-2])
        self.g.xq = np.append(self.g.xq, 2*self.g.xq[-1] - self.g.xq[-2])
        # Note that this requires u[i] -> u[i+1]
        # to compensate for velocity cell on inner boundary

        self.h = self.g.q()
        """ Fluid depth (m) """
        self.hvt = self.g.u()
        """ Azimuthal transport (m*m/s) """
        self.hvr = self.g.u()
        """ Radial transport (m*m/s) """
        self.Aq = 0.5 * (self.g.xu[1:-1]**2 - self.g.xu[:-2]**2)
        self.Aq[0] = 0.5 * self.g.xu[1]**2
        """ Area of tracer cells (m*m) """
        self.Au = 0.5 * (self.g.xq[1:]**2 - self.g.xq[:-1]**2)
        """ Area of velocity cells (m*m) """

    def zero(self):
        """
        Set current time tendency to 0
        """
        self.hvrt[0,:] = 0.
        self.hvtt[0,:] = 0.
        self.ht[0,:] = 0.

    def rotate(self):
        """ Rotate the time tendency arrays """
        self.hvrt = np.roll(self.hvrt, 1, axis = 0)
        self.hvtt = np.roll(self.hvtt, 1, axis = 0)
        self.ht = np.roll(self.ht, 1, axis = 0)

    def boundaries(self):
        """
        Fill ghost cells with appropriate values.

        For this model, the required ghost cells are at r_0^u, r_{n+1}^q, and
        r_{n+1}^u. We
        use the azimuthal symmetry of the model to determine the velocities at
        r_0^u, set the height at r_{n+1}^q to a constant height H_0, and set
        the velocities at r_{n+1}^u to 0.
        """
        self.hvr[0] = self.hvr[1]
        self.hvt[0] = self.hvt[1]
        self.hvr[-1] = 0.
        self.hvt[-1] = 0.
        self.h[-1] = self.H0

    def advection(self):
        """
        Compute advective fluxes and add them to tendency arrays
        """

        # Height fluxes
        self.ht[0,:] = self.ht[0,:] - (self.g.xu[1:-1]*self.hvr[1:-1] -
                        self.g.xu[:-2]*self.hvr[:-2]) / self.Aq

        # Compute radial velocities at velocity cell boundaries
        vrq = 0.5 * (self.hvr[1:] + self.hvr[:-1]) / self.h
        vrp = np.where(vrq > 0, vrq, 0.)
        vrm = np.where(vrq <= 0, vrq, 0.)

        # Transport fluxes
        self.hvrt[0,:] = self.hvrt[0,:] + (
                self.g.xq[1:] * (vrm[1:] * self.hvr[2:] +
                               vrp[1:] * self.hvr[1:-1]) -
                self.g.xq[:-1] * (vrm[:-1] * self.hvr[1:-1] +
                                vrp[:-1] * self.hvr[:-2])) / self.Au
        self.hvtt[0,:] = self.hvtt[0,:] + (
                self.g.xq[1:] * (vrm[1:] * self.hvt[2:] +
                               vrp[1:] * self.hvt[1:-1]) -
                self.g.xq[:-1] * (vrm[:-1] * self.hvt[1:-1] +
                                vrp[:-1] * self.hvt[:-2])) / self.Au

    def coriolis(self):
        """
        Compute tendencies from coriolis accelerations
        """
        self.hvrt[0,:] = self.hvrt[0,:] + self.f * self.hvt[1:-1]
        self.hvtt[0,:] = self.hvtt[0,:] - self.f * self.hvr[1:-1]

    def heightgrad(self):
        """
        Compute tendencies from height gradients
        """
        self.hvrt[0,:] = self.hvrt[0,:] - self.grav/2. * (
                (self.h[1:]**2 - self.h[:-1]**2) /
                (self.g.xq[1:] - self.g.xq[:-1]))

    def damping(self, r, dampt, dampr):
        """
        Include damping outside of a certain radius that is proportional to
        the transport and scales like a gaussian with the distance from r.

        Parameters
        ----------
        r: float
            Radius at which the damping band starts

        dampt: float
            Damping timescale (s)

        dampr: float
            Damping spatial pickup scale (m)
        """
        dampt = 1/dampt
        self.hvrt[0,:] = self.hvrt[0,:] - \
                np.where(self.g.xu[1:-1] > r,
                    dampt*(1 - np.exp(-(self.g.xu[1:-1] - r)**2/dampr**2))
                    * self.hvr[1:-1], 0)
        self.hvtt[0,:] = self.hvtt[0,:] - \
                np.where(self.g.xu[1:-1] > r,
                    dampt*(1 - np.exp(-(self.g.xu[1:-1] - r)**2/dampr**2))
                    * self.hvt[1:-1], 0)


    def forcing(self):
        """
        Compute tendencies from any other forcing terms.
        Can be overridden in subclasses.
        """
        pass

    def step(self, dt):
        """
        Integrate fields forward by one time step

        Parameters
        ----------
        dt: float
            time step (s)
        """
        # Compute Adams-Bashforth coefficients
        if self.n > 1:
            abc = [23./12., -4./3., 5./12.]
        elif self.n > 0:
            abc = [1.5, -0.5, 0]
        else:
            abc = [1., 0., 0.]

        # Step
        self.hvr[1:-1] = self.hvr[1:-1] + dt * (
                         abc[0]*self.hvrt[0,:] +
                         abc[1]*self.hvrt[1,:] +
                         abc[2]*self.hvrt[2,:])
        self.hvt[1:-1] = self.hvt[1:-1] + dt * (
                         abc[0]*self.hvtt[0,:] +
                         abc[1]*self.hvtt[1,:] +
                         abc[2]*self.hvtt[2,:])
        self.h[0:-1] = self.h[0:-1] + dt * (
                       abc[0]*self.ht[0,:] +
                       abc[1]*self.ht[1,:] +
                       abc[2]*self.ht[2,:])
        self.t += dt
        self.n += 1

    def get_h(self):
        """
        Returns
        -------
        array-like: height field
        """
        return self.h[:-1]

    def get_ht(self):
        """
        Returns
        -------
        array-like: height tendency field
        """
        return self.ht[0,:]

    def get_hvr(self):
        """
        Returns
        -------
        array-like: radial transport field
        """
        return self.hvr[1:-1]

    def get_hvrt(self):
        """
        Returns
        -------
        array-like: radial transport tendency field
        """
        return self.hvrt[0,:]

    def get_hvt(self):
        """
        Returns
        -------
        array-like: azimuthal transport field
        """
        return self.hvt[1:-1]

    def get_hvtt(self):
        """
        Returns
        -------
        array-like: azimuthal transport tendency field
        """
        return self.hvtt[0,:]

    def get_xq(self):
        """
        Returns
        -------
        array-like: coordinates at tracer cell centers
        """
        return self.g.xq[:-1]

    def get_xu(self):
        """
        Returns
        -------
        array-like: coordinates at velocity cell centers
        """
        return self.g.xu[1:-1]

    def ploth(self):
        """
        Make a plot of the height field
        """
        l, = plt.plot(self.get_xq(), self.get_h())
        plt.xlabel('r (m)')
        plt.ylabel('fluid depth (m)')
        return l


    def plothvt(self):
        """
        Make a plot of the azimuthal transport field
        """
        l, = plt.plot(self.get_xu(), self.get_hvt())
        plt.xlabel('r (m)')
        plt.ylabel('azimuthal transport (m^2/s)')
        return l

    def plothvr(self):
        """
        Make a plot of the radial transport field
        """
        l, = plt.plot(self.get_xu(), self.get_hvr())
        plt.xlabel('r (m)')
        plt.ylabel('radial transport (m^2/s)')
        return l

    def plotht(self):
        """
        Make a plot of the height tendency field
        """
        l, = plt.plot(self.get_xq(), self.get_ht())
        plt.xlabel('r (m)')
        plt.ylabel('fluid depth tendency (m/s)')
        return l

    def plothvtt(self):
        """
        Make a plot of the azimuthal transport tendency field
        """
        l, = plt.plot(self.get_xu(), self.get_hvtt())
        plt.xlabel('r (m)')
        plt.ylabel('azimuthal transport tendency (m^2/s^2)')
        return l

    def plothvrt(self):
        """
        Make a plot of the radial transport tendency field
        """
        l, = plt.plot(self.get_xu(), self.get_hvrt())
        plt.xlabel('r (m)')
        plt.ylabel('radial transport tendency (m^2/s^2)')
        return l
